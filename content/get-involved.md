---
layout: get-involved
title: Get Involved
name: KDE Eco
userbase: KDE Eco
menu:
  main:
    weight: 4
getintouch: |
  Most development-related discussions take place at our [Matrix room](https://webchat.kde.org/#/room/#energy-efficiency:kde.org) or the energy efficiency [mailing list](https://mail.kde.org/cgi-bin/mailman/listinfo/energy-efficiency).
  Join in and tell us what you would like to help us with!
---

Want to contribute? Check out the GitLab repositories for [FEEP](https://invent.kde.org/cschumac/feep) or [BE4FOSS](https://invent.kde.org/joseph/be4foss).
